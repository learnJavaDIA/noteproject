package com.noteproject;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.TextView;
import android.text.Html;
import java.io.IOException;
import java.io.InputStream;

/**
 * This activity perform view article content.
 * @author D.I.A.
 */
public class SubActivity extends Activity{
	

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.sub_layout);

        TextView textView = (TextView)findViewById(R.id.txt_field);
		
        String fileName = getIntent().getStringExtra(MainActivity.FILE_NAME);
        textView.setText(Html.fromHtml(getStringFromAssets(fileName)));
    }

    /**
     * Read and return content from file.
     * @param fileName - file name in assets folder.
     * @return - content from file.
     */
    private String getStringFromAssets(String fileName){
        try {
            InputStream is = getAssets().open(fileName);
            byte[] buffer = new byte[is.available()];

            if(is.read(buffer) < 1){
                return null;
            }

            is.close();

            return new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}