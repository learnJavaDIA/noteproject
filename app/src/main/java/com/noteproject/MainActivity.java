package com.noteproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.Nullable;

/**
 * class that transfers the file name to the new Activity
 *
 * @author Voevodin A. A.
 */
public class MainActivity extends Activity {

    // constant to identify the value passed to SubActivity
    public static final String FILE_NAME = "file_name";

    /**
     * Initialization components activity_main.xml and implementation of sending a file name in
     * SubActivity based on the received data
     *
     * @param savedInstanceState If the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in {@link #onSaveInstanceState}.  <b><i>Note: Otherwise it is null.</i></b>
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String[] themes = getResources().getStringArray(R.array.themes);
        final String[] fileNames = getResources().getStringArray(R.array.fileNames);
        final ListView listThemes = findViewById(R.id.lvContent);

        final ArrayAdapter adapter = new ArrayAdapter<>(this, R.layout.list_item, themes);
        listThemes.setAdapter(adapter);

        listThemes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                Intent intent = new Intent(MainActivity.this, SubActivity.class);
                intent.putExtra(FILE_NAME, fileNames[position]);
                startActivity(intent);
            }
        });
    }
}
